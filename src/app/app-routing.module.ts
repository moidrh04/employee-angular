import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormularioComponent } from './pages/formulario/formulario.component';

const routes: Routes = [
  {
    path: 'crear',
    component: FormularioComponent
  },
  {path: '**', pathMatch: 'full', redirectTo: 'crear'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
