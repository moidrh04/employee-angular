import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  buscarAllEmployees() {
    const url = `${base_url}/getAllEmployees`;
    return this.http.get<any[]>(url).pipe();
  }

  saveEmployee(fullname: string, empfunct: string){

    const values = {
      fullname,
      empfunct
    };

    const url = `${base_url}/create`;
    return this.http.post<any[]>(url, { }, {params: values}).pipe(
      map(resp=>{return resp})
    );
  }
}
