import { Component, OnInit } from '@angular/core';
import { ControlContainer, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee.model';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  forma: FormGroup;

  public employees: Employee[] = [];

  public employee = new Employee();

  constructor( private fb: FormBuilder, private EmployeeService: EmployeeService) {

    this.crearFormulario();

  }

  ngOnInit(): void {
  }

  get nombreNoValido() {
    return this.forma.get('fullname').invalid && this.forma.get('fullname').touched;
  }

  get funNoValido() {
    return this.forma.get('empfunct').invalid && this.forma.get('empfunct').touched;
  }

  crearFormulario() {

    this.forma = this.fb.group({
      fullname: ['', [Validators.required, Validators.minLength(5)]],
      empfunct: ['', [Validators.required, Validators.minLength(3)]]
    });

  }

  guardar() {

    if( this.forma.invalid) {
      Object.values(this.forma.controls).forEach(
        c=>{c.markAsTouched()}
      );
    } else {
      this.EmployeeService.saveEmployee(this.employee.fullname, this.employee.empfunct)
          .subscribe();

    this.forma.reset();
    }
    
  }

  searchAllEmployees(){
    this.EmployeeService.buscarAllEmployees()
        .subscribe(e=>{
          this.employees = e;
        });
  }

  clean(){
    this.employees = [];
  }

}
